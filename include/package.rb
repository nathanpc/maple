# package.rb
# Handling the packages stuff.

require 'rubygems'
require 'yaml'
require 'term/ansicolor'
require 'net/http'
require 'uri'

class Package
	private
	# Parse the index file.
	def self.parse_index
		packages = []

		file = File.open("./package.list", "r")
		index = file.read
		file.close

		index = index.split("\n")
		index.each do |pkg|
			pkg = pkg.split(" - ")

			packages.push({
				"name" => pkg[0],
				"description" => pkg[1]
			})
		end

		return packages
	end

	# Parse a package YAML description.
	def self.parse_description(url)
		resp = Net::HTTP.get_response(URI.parse(url))

		if resp.kind_of? Net::HTTPOK
			return YAML.load(resp.body)
		else
			abort "Could not connect to \"#{url}\"".red
		end
	end

	public
	# Search the index for a desired package.
	def self.search_index(pkg)
		parse_index().each do |package|
			if package["name"] == pkg
				return package
			end
		end

		puts "Couldn't find any packages named \"#{pkg}\"".red
		return false
	end

	# Install a package.
	def self.install(pkg)
		pkg = search_index(pkg)

		if pkg != false
			package = parse_description(pkg["description"])

			if !package["dependencies"].nil?
				puts "We got dependencies!"
				package["dependencies"].each do |dependency|
					puts "    - #{dependency}"
				end
				print "\n"
			end

			# Replace the version in the download string.
			package["download"].sub! "#version#", package["version"]

			puts "Download: #{package["download"]}"
		end
	end
end
