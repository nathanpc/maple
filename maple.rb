#!/usr/bin/env ruby
# maple.rb
# The awesome cross-platform source package manager

require 'rubygems'
require 'term/ansicolor'

require './include/package.rb'

# Making Term::ANSIColor easier to use.
class String
    include Term::ANSIColor
end

# The usual "Usage" message.
def usage
	puts "maple 0.1 (c) 2013 Nathan Campos"
	puts "The awesome cross-platform source package manager."

	puts "\nUsage: ".bold + "maple".green + " command [options]"

	puts "\nCommands".bold
	puts "    install\tInstall a package"
	puts "    help\tThis"
end

# Parsing the command-line arguments.
def cmd_args
	case ARGV[0]
		when "install"
			if !ARGV[1].nil?
				Package.install(ARGV[1])
			else
				puts "You need to specify a package to install.".red
				puts "Usage: ".bold + "maple".green + " install [package]"
			end
		when "help"
			usage()
		else
			usage()
	end
end


# Run!
if __FILE__ == $0
	if ARGV[0].nil?
        usage()
    else
        cmd_args()
    end
end
